function main (const p : int ; const s : int) : (list(operation) * int) is
  block {skip} with ((nil : list(operation)), s + 1)
